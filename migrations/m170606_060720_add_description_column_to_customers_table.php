<?php

use yii\db\Migration;

/**
 * Handles adding description to table `customers`.
 */
class m170606_060720_add_description_column_to_customers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customers', 'discription', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('customers', 'discription');
    }
}
